from django.db import models
from django.core.files import File

from io import BytesIO
from PIL import Image

from core.models import Consumer, Supplier

class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ('name',)

    def __str__(self):
        return self.name

class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    description = models.TextField(blank=True, null=True)
    price = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    thumbnail = models.ImageField(upload_to='uploads/', blank=True, null=True)
    added_by = models.ForeignKey(Supplier, related_name='products', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created_at',)
        #ordering = ('-price',)
        
    def __str__(self):
        return self.name

    def get_display_price(self):
        return self.price / 100

    def get_thumbnail(self):
        if self.thumbnail:
            return self.thumbnail.url
        else:
            if self.image:
                self.thumbnail = self.make_thumbnail(self.image)
                self.save()

                return self.thumbnail.url
            else:
                return 'https://via.placeholder.com/240x240x.jpg'

    def make_thumbnail(self, image, size=(300, 300)):
        img = Image.open(image)
        img.convert('RGB')
        img.thumbnail(size)

        thumb_io = BytesIO()
        img.save(thumb_io, 'JPEG', quality=85)

        thumbnail = File(thumb_io, name=image.name)

        return thumbnail
    
    def get_rating(self):
        reviews_total = 0
        questions = 0

        for review in self.reviews.all():
            if review.rating != 0:  # review with rating == 0 not count
                reviews_total += review.rating
            else:
                questions = questions + 1  # self.reviews.count() - questions 

        if reviews_total > 0:
            return reviews_total / (self.reviews.count() - questions)
        
        return 0

    def get_rewiew_count(self): 
        return self.reviews.count()
            

class Review(models.Model):
    product = models.ForeignKey(Product, related_name='reviews', on_delete=models.CASCADE)
    rating = models.IntegerField(default=3)
    content = models.TextField()
    created_by = models.ForeignKey(Consumer, related_name='reviews', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    answer = models.TextField(blank=True) #answered only by his supplier
