from django.shortcuts import render, get_object_or_404, redirect

from .models import Product, Review
from core.models import Consumer #Supplier

def product(request, slug):
    product = get_object_or_404(Product, slug=slug)

    if request.method == 'POST':
        rating = request.POST.get('rating', 3)
        content = request.POST.get('content', '')
        answer = request.POST.get('answer', '')
        rev = request.POST.get('review_id', '')

        if content:
            reviews = Review.objects.filter(created_by=request.user.id, product=product) #request.user

            consumer = Consumer.objects.get(user_id=request.user.id)
            
            if reviews.count() > 0:
                review = reviews.first()
                review.rating = rating
                review.content = content
                review.save()
            else:
                review = Review.objects.create(
                    product=product,
                    rating=rating,
                    content=content,
                    #created_by=request.user,
                    created_by=consumer,
                )

            return redirect('product', slug=slug)


        if answer:
            review = Review.objects.get(id=rev) #id review to update
            review.answer=answer
            review.save()
            return redirect('product', slug=slug)
    

    return render(request, 'product/product.html', {'product':product})