from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_consumer = models.BooleanField(default=False)
    is_supplier = models.BooleanField(default=False)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)    
    email = models.CharField(max_length=100)

class Consumer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    genre = models.ManyToManyField(related_name='genre', to='product.Category')  

class Supplier(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    phone_number = models.CharField(max_length=20)
    designation = models.CharField(max_length=20)