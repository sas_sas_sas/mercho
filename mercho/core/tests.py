from django.test import TestCase, Client
from django.urls import reverse

from product.models import Category, Product
from .models import User, Consumer, Supplier


class testCore(TestCase):  

    '''test on User Model'''

    def setUp(self):
        self.user2 = User.objects.create(
            is_consumer="True", 
            is_supplier="False", 
            username='consumer1', 
            first_name="a", 
            last_name="b", 
            email="a@gmail.com",
        )

    def test_obj(self):
        self.assertTrue(
            isinstance(self.user2, User), 
            "User2 is not a User"
        )

    def test_user_is_consumer(self):
        self.assertEqual(self.user2.is_consumer, 'True') 
        
    def test_user_is_not_supplier(self):
        self.assertEqual(self.user2.is_supplier, 'False') 



class TestViews(TestCase):

    '''test on some Views'''

    def setUp(self):
        self.frontpage_url = reverse('frontpage')

        self.shop_product_url = reverse('product', args=['p-1'])
        user1 = User.objects.create(
            is_consumer="False", 
            is_supplier="True", 
            username='supplier1', 
            first_name="c", 
            last_name="d", 
            email="c@gmail.com",
        )
        supplier1 = Supplier.objects.create(
            user=user1,
            phone_number='1',
            designation='mi',
        )
        category1 = Category.objects.create(
            name='Pop',
            slug="pop",
        )
        self.product1 = Product.objects.create(   # image and created_at not used
            category=category1,   
            name='p1',
            slug='p-1',
            description='desc',
            price=1000, 
            added_by=supplier1,  
        )

        self.edit_my_account_url = reverse('edit_myaccount')


    def test_frontpage_as_consumer(self):
        response = self.client.get(self.frontpage_url)

        self.assertEqual(response.status_code, 200)  # OK
        self.assertTemplateUsed(response, 'core/frontpage.html')

    def test_shop_product(self):
        response = self.client.get(self.shop_product_url)

        self.assertEqual(response.status_code, 200)  # OK
        self.assertTemplateUsed(response, 'product/product.html')
    
    def test_edit_my_account_and_login(self):
        user = User.objects.create(username="test")
        user.set_password("test_psw00")
        user.is_consumer = True
        user.save()

        Consumer.objects.create(user=user)

        c = Client()
        response = c.post('/login/', {'username': 'test', 'password': 'test_psw00'})
        self.assertEqual(response.status_code, 302)  # redirect 302 OK

        response2 = c.post(self.edit_my_account_url, data={
            'first_name': 'prova',
            'last_name': 'consumer',
            'email': 'prova@gmail.com',
            'username': 'provaconsumer',})
        self.assertEqual(response2.status_code, 302)  # redirect 302 OK
        self.assertTemplateNotUsed(response, 'core/edit_myaccount.html')

        response3 = c.post('/login/', {'username': 'provaconsumer', 'password': 'test_psw00'})
        self.assertEqual(response3.status_code, 302)  # redirect 302 OK

        response4 = c.post('/login/', {'username': 'john', 'password': 'smith'})  # not exist
        self.assertEqual(response4.status_code, 200)  # login error 

    def test_my_account_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.is_supplier = True  # User supplier
        user.save()

        supplier = Supplier.objects.create(
            user=user,
            phone_number='1',
            designation='mi',
        )

        c = Client()
        logged_in = c.login(username='testuser', password='12345')

        self.assertTrue(logged_in)

        response = c.get(reverse("myaccount")) # user logged -> myaccount
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['products'], [])


        category = Category.objects.create(
            name='Pop',
            slug="pop",
        )
        
        Product.objects.create(   # image and created_at not used
            category=category,   
            name='p1',
            slug='p-1',
            description='desc',
            price=1000, 
            added_by=supplier,  
        )

        response2 = c.get(reverse("myaccount")) # user logged -> myaccount
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response2.context['products'].count(), 1)
