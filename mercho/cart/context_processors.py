'''
A context processor is a Python function that takes the request object as an argument and 
returns a dictionary that gets added to the request context. 
They come in handy when you need to make something available globally to all templates.
'''
from .cart import Cart

def cart(request):
    return {'cart': Cart(request)}